#include <stdio.h>

int main() {
	int i,j;
    float notas[15][3];
    float mediaTurma = 0,mediaAluno = 0;

    // L� a matriz de notas
    printf("Digite as notas dos alunos (15x3):\n");
    for (i = 0; i < 15; i++) {
        printf("Notas do aluno %d: ", i + 1);
        for ( j = 0; j < 3; j++) {
            scanf("%f", &notas[i][j]);
        }
    }

    // Calcula as m�dias dos alunos e da turma
    for ( i = 0; i < 15; i++) {
        
        for ( j = 0; j < 3; j++) {
            mediaAluno += notas[i][j];
        }
        mediaAluno /= 3.0;
        printf("M�dia do aluno %d: %.2f\n", i + 1, mediaAluno);

        // Adiciona � m�dia da turma
        mediaTurma=mediaTurma+ mediaAluno;
    }

    // Calcula e mostra a m�dia da turma
    mediaTurma /= 15.0;
    printf("M�dia da turma: %.2f\n", mediaTurma);
    
    
    system("pause");
    return 0;
}

